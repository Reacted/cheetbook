import React from 'react'
import CardContainer from './card';

class App extends React.Component {
    render() {
        return (
            <CardContainer />
        )
    }
}

export default App;